﻿using System;
using System.Collections.Generic;

namespace nIceGame
{
    public interface ISystem : IDisposable
    {
        void OnResize();
        void OnUpdate(double delta);
        void OnRender(double delta);
    }
}
