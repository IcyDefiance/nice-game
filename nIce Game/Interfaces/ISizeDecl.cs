﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceGame
{
    public interface ISizeDecl
    {
        int SizeInBytes { get; }
    }
}
