﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace nIceGame
{
    abstract public class Scene : IDisposable
    {
        private bool disposed = false;

        public virtual void OnResize(object sender, System.EventArgs e)
        {
            GL.Viewport(Engine.GameWindow.ClientRectangle);
            RenderTarget2D.BindTarget(null);

            for (int i = 0; i < Engine.Systems.Count; i++)
                Engine.Systems[i].OnResize();
        }

        public virtual void OnLoad()
        {

        }

        public virtual void UpdateFrame(object sender, FrameEventArgs e)
        {
            for (int i = 0; i < Engine.Systems.Count; i++)
                Engine.Systems[i].OnUpdate(e.Time);
        }

        public virtual void RenderFrame(object sender, FrameEventArgs e)
        {
            for (int i = 0; i < Engine.Systems.Count; i++)
                Engine.Systems[i].OnRender(e.Time);
        }

        public virtual void OnLoseFocus()
        {

        }

        public virtual void OnRegainFocus()
        {

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                for (int i = 0; i < Engine.Systems.Count; i++)
                {
                    Engine.Systems[i].Dispose();
                }
            }

            disposed = true;
        }

        ~Scene()
        {
            Dispose(false);
        }
    }
}
