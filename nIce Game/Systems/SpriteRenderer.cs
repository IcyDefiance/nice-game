﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace nIceGame
{
    public class SpriteRenderer : ISystem, IDisposable
    {
        private List<Entity> spriteCache, textCache;
        private Shader shader;
        private int vbo, vao;
        private SpriteFont spriteFont;
        private bool spriteZOrderDirty, disposed;

        public SpriteRenderer()
        {
            spriteCache = new List<Entity>();

            shader = new Shader();
            shader.VertexSource = PresetShaders.SpriteVS;
            shader.FragmentSource = PresetShaders.SpriteFS;
            shader.Compile();

            vbo = GL.GenBuffer();
            vao = GL.GenVertexArray();

            GL.BindVertexArray(vao);

            SpriteElement[] quadPoints = new SpriteElement[4];
            for (int i = 0; i < quadPoints.Length; i++)
                quadPoints[i] = new SpriteElement();
            quadPoints[0].Vertex = new Vector2(0, 0);
            quadPoints[0].TexCoord = Vector2.UnitY;
            quadPoints[1].Vertex = new Vector2(0, -2);
            quadPoints[1].TexCoord = Vector2.Zero;
            quadPoints[2].Vertex = new Vector2(2, 0);
            quadPoints[2].TexCoord = Vector2.One;
            quadPoints[3].Vertex = new Vector2(2, -2);
            quadPoints[3].TexCoord = Vector2.UnitX;

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (System.IntPtr)(quadPoints[0].SizeInBytes * quadPoints.Length), quadPoints, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, BlittableValueType.StrideOf(quadPoints), 0);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, BlittableValueType.StrideOf(quadPoints), Vector2.SizeInBytes);

            spriteFont = new SpriteFont();
        }

        public void OnResize()
        {
        }

        public void OnUpdate(double delta)
        {
            if (!Engine.UpdateCache.ContainsKey(typeof(Texture2D)) || Engine.UpdateCache[typeof(Texture2D)])
                UpdateSpriteCache();
            if (!Engine.UpdateCache.ContainsKey(typeof(Text)) || Engine.UpdateCache[typeof(Text)])
                UpdateTextCache();
        }

        public void OnRender(double delta)
        {
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.CullFace);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            shader.Use();
            GL.BindVertexArray(vao);
            GL.ActiveTexture(TextureUnit.Texture0);

            if (spriteCache != null && spriteCache.Count != 0)
                DrawSprites();
            RenderTarget2D.BindTarget(null);

            if (textCache != null && textCache.Count != 0)
                DrawTexts();
            RenderTarget2D.BindTarget(null);

            GL.Disable(EnableCap.Blend);
            GL.Enable(EnableCap.CullFace);
            GL.Enable(EnableCap.DepthTest);
        }

        private void UpdateSpriteCache()
        {
            if (spriteCache != null)
                for (int i = 0; i < spriteCache.Count; i++)
                    spriteCache[i].Get<Transform>().PositionChanged -= SpriteRenderer_PositionChanged;

            spriteCache = Engine.Entities.FindAll(x => x.Get<Texture2D>() != null && x.Get<Texture2D>().IsSprite);
            spriteCache.Sort(TransformDepthComparison);
            for (int i = 0; i < spriteCache.Count; i++)
                spriteCache[i].Get<Transform>().PositionChanged += SpriteRenderer_PositionChanged;

            Engine.UpdateCache[typeof(Texture2D)] = false;
        }

        void SpriteRenderer_PositionChanged(object sender, EventArgs e)
        {
            spriteZOrderDirty = true;
        }

        private void UpdateTextCache()
        {
            textCache = Engine.Entities.FindAll(x => x.Get<Text>() != null);
            textCache.Sort(TransformDepthComparison);
            Engine.UpdateCache[typeof(Text)] = false;
        }

        private void DrawSprites()
        {
            if (spriteZOrderDirty)
            {
                spriteCache.Sort(TransformDepthComparison);
                spriteZOrderDirty = false;
            }

            RenderTarget2D lastTarget = null;
            for (int i = 0; i < spriteCache.Count; i++)
            {
                RenderTarget2D currentTarget = spriteCache[i].Get<RenderTarget2D>();
                if (!object.Equals(lastTarget, currentTarget))
                {
                    RenderTarget2D.BindTarget(currentTarget);
                    lastTarget = currentTarget;
                }

                Texture2D sprite = spriteCache[i].Get<Texture2D>();
                Transform transform = spriteCache[i].Get<Transform>();
                Tint tint = spriteCache[i].Get<Tint>();
                if (tint == null)
                    tint = Tint.White;

                DrawTexture2D(sprite, transform, tint);
            }
        }

        private void DrawTexts()
        {
            RenderTarget2D lastTarget = null;
            for (int i = 0; i < textCache.Count; i++)
            {
                RenderTarget2D currentTarget = textCache[i].Get<RenderTarget2D>();
                if (!object.Equals(lastTarget, currentTarget))
                {
                    RenderTarget2D.BindTarget(currentTarget);
                    lastTarget = currentTarget;
                }

                Text text = textCache[i].Get<Text>();
                Transform transform = textCache[i].Get<Transform>() != null ? textCache[i].Get<Transform>() : new Transform(Vector3.Zero, Quaternion.Identity, Vector3.One);
                Transform currentTransform = new Transform(transform.Position, transform.Rotation, transform.Scale);
                Tint tint = textCache[i].Get<Tint>();
                if (tint == null)
                    tint = Tint.White;

                float origCurPosX = currentTransform.Position.X;

                for (char j = (char)0; j < text.String.Length; j++)
                {
                    if (text.String[j] == '\n')
                        currentTransform.Position = new Vector3(origCurPosX, currentTransform.Position.Y + spriteFont.AdvanceHeight, currentTransform.Position.Z);
                    else
                    {
                        DrawTexture2D(spriteFont.Glyphs[text.String[j]].Texture, currentTransform, tint);
                        currentTransform.Position += new Vector3((float)spriteFont.Glyphs[text.String[j]].AdvanceWidth, 0, 0);
                    }
                }
            }
        }

        Vector2 lastPos, lastScale, lastTexSize, lastTarSize;
        Tint lastTint = new Tint(0, 0, 0, 1);
        int lastTextureID = -1;
        private void DrawTexture2D(Texture2D texture, Transform transform, Tint tint)
        {
            if (transform.Position.Xy != lastPos)
            {
                lastPos = transform.Position.Xy;
                shader.SetUniform("pos", lastPos);
            }
            if (transform.Scale.Xy != lastScale)
            {
                lastScale = transform.Scale.Xy;
                shader.SetUniform("scale", lastScale);
            }
            if (texture.Width != lastTexSize.X || texture.Height != lastTexSize.Y)
            {
                lastTexSize = new Vector2(texture.Width, texture.Height);
                shader.SetUniform("texSize", lastTexSize);
            }
            if (RenderTarget2D.CurrentTargetSize != lastTarSize)
            {
                lastTarSize = RenderTarget2D.CurrentTargetSize;
                shader.SetUniform("tarSize", lastTarSize);
            }
            if (tint.R != lastTint.R || tint.G != lastTint.G || tint.B != lastTint.B || tint.W != lastTint.W)
            {
                shader.SetUniform("tint", tint.Vec4);
                lastTint = new Tint(tint.R, tint.G, tint.B, tint.W);
            }
            if (texture.TexID != lastTextureID)
            {
                GL.BindTexture(TextureTarget.Texture2D, texture.TexID);
                lastTextureID = texture.TexID;
            }

            GL.DrawArrays(BeginMode.TriangleStrip, 0, 4);
        }

        public int TransformDepthComparison(Entity a, Entity b)
        {
            return a.Get<Transform>().Position.Z.CompareTo(b.Get<Transform>().Position.Z);
        }

        private static class PresetShaders
        {
            public static string SpriteVS = @"#version 330
                    in layout (location = 0) vec2 vertPos;
                    in layout (location = 1) vec2 vertTexcoord;
                    out vec2 texcoord;

                    uniform vec2 pos, scale, texSize, tarSize;

                    void main() {
                        vec2 realPos = vec2(2 * pos.x / tarSize.x - 1, -2 * pos.y / tarSize.y + 1);
                        vec2 realScale = vec2(texSize.x / tarSize.x, texSize.y / tarSize.y) * scale;

                        texcoord = vertTexcoord;
                        gl_Position = vec4(realScale * vertPos + realPos, 0.0, 1.0);
                    }";

            public static string SpriteFS = @"#version 330
                    in vec2 texcoord;

                    uniform sampler2D texture;
                    uniform vec4 tint;

                    out vec4 fragColor;

                    void main() {
                        fragColor = texture2D(texture, texcoord) * tint;
                    }";
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                shader.Dispose();
                spriteFont.Dispose();
            }

            disposed = true;
        }

        ~SpriteRenderer()
        {
            Dispose(false);
        }

        [StructLayout(LayoutKind.Sequential)]
        struct SpriteElement : ISizeDecl
        {
            public Vector2 Vertex;
            public Vector2 TexCoord;

            public int SizeInBytes
            {
                get
                {
                    return Vector2.SizeInBytes * 2;
                }
            }
        }
    }
}
