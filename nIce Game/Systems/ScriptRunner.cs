﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceGame
{
    public class ScriptRunner : ISystem
    {
        private List<Entity> scriptCache;

        public ScriptRunner()
        {
            scriptCache = new List<Entity>();
        }

        public void OnResize()
        {

        }

        public void OnUpdate(double delta)
        {
            if (!Engine.UpdateCache.ContainsKey(typeof(List<Script>)) || Engine.UpdateCache[typeof(List<Script>)])
                UpdateScriptCache();

            for (int i = 0; i < scriptCache.Count; i++)
            {
                List<Script> scripts = scriptCache[i].Get<List<Script>>();
                for (int j = 0; j < scripts.Count; j++)
                    if (scripts[j] != null)
                        scripts[j].Update(delta);
            }
        }

        public void OnRender(double delta)
        {
            for (int i = 0; i < scriptCache.Count; i++)
            {
                List<Script> scripts = scriptCache[i].Get<List<Script>>();
                for (int j = 0; j < scripts.Count; j++)
                    if (scripts[j] != null)
                        scripts[j].Draw(delta);
            }
        }

        private void UpdateScriptCache()
        {
            scriptCache = Engine.Entities.FindAll(x => x.Get<List<Script>>() != null);
            Engine.UpdateCache[typeof(List<Script>)] = false;
        }

        public void Dispose()
        {
        }
    }
}
