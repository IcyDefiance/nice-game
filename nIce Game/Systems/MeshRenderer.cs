﻿using System;
using System.Linq;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace nIceGame
{
    public class MeshRenderer : ISystem, IDisposable
    {
        private List<Entity> meshCache;
        private List<Entity> meshBlendCache;
        private List<Entity> cameraCache;
        private Shader shader;
        private bool disposed = false;

        public MeshRenderer()
        {
            meshCache = new List<Entity>();
            cameraCache = new List<Entity>();

            shader = new Shader();
            shader.VertexSource = PresetShaders.ModelVS;
            shader.FragmentSource = PresetShaders.ModelFS;
            shader.Compile();
        }

        public void OnResize()
        {

        }

        public void OnUpdate(double delta)
        {
            if (!Engine.UpdateCache.ContainsKey(typeof(Mesh)) || Engine.UpdateCache[typeof(Mesh)])
                UpdateMeshCache();
            if (!Engine.UpdateCache.ContainsKey(typeof(Camera)) || Engine.UpdateCache[typeof(Camera)])
                UpdateCameraCache();
        }

        public void OnRender(double delta)
        {
            if (meshCache == null || cameraCache == null) return;
            
            for (int i = 0; i < cameraCache.Count; i++)
            {
                Camera camera = cameraCache[i].Get<Camera>();
                Transform viewTransform = cameraCache[i].Get<Transform>();

                for (int j = 0; j < meshCache.Count; j++)
                {
                    Mesh mesh = meshCache[j].Get<Mesh>();
                    Transform modelTransform = meshCache[j].Get<Transform>();
                    Material material = meshCache[j].Get<Material>();
                    DrawMesh(mesh, material, camera, viewTransform, modelTransform);
                }

                GL.Enable(EnableCap.Blend);
                BlendFunction lastFunction = null;
                for (int j = 0; j < meshBlendCache.Count; j++)
                {
                    BlendFunction currentFunction = meshBlendCache[j].Get<BlendFunction>();
                    if (lastFunction == null || currentFunction.blendingFactorSrc != lastFunction.blendingFactorSrc || currentFunction.blendingFactorDest != lastFunction.blendingFactorDest)
                    {
                        GL.BlendFunc(currentFunction.blendingFactorSrc, currentFunction.blendingFactorDest);
                        lastFunction = currentFunction;
                    }

                    Mesh mesh = meshBlendCache[j].Get<Mesh>();
                    Transform modelTransform = meshBlendCache[j].Get<Transform>();
                    Material material = meshBlendCache[j].Get<Material>();
                    DrawMesh(mesh, material, camera, viewTransform, modelTransform);
                }
                GL.Disable(EnableCap.Blend);
            }
        }

        private void UpdateMeshCache()
        {
            meshCache = Engine.Entities.FindAll(x => x.Get<Mesh>() != null);
            meshBlendCache = meshCache.FindAll(x => x.Get<BlendFunction>() != null);
            meshCache = meshCache.Except(meshBlendCache).ToList();
            Engine.UpdateCache[typeof(Mesh)] = false;
        }

        private void UpdateCameraCache()
        {
            cameraCache = Engine.Entities.FindAll(x => x.Get<Camera>() != null);
            Engine.UpdateCache[typeof(Camera)] = false;
        }

        private void DrawMesh(Mesh mesh, Material material, Camera camera, Transform viewTransform, Transform modelTransform)
        {
            if (mesh == null) return;

            shader.Use();
            shader.SetUniform("viewMat", viewTransform.InverseMatrix);
            shader.SetUniform("projMat", camera.Projection);
            shader.SetUniform("worldMat", modelTransform.Matrix);
            shader.SetUniform("Ks", material.SpecularColor);
            shader.SetUniform("Ka", material.AmbientColor);
            shader.SetUniform("specWeight", material.SpecularWeight);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, material.DiffuseMap.TexID);

            GL.BindVertexArray(mesh.VAO);
            GL.DrawElements(BeginMode.Triangles, mesh.ElementCount, DrawElementsType.UnsignedInt, (System.IntPtr)0);
        }

        private static class PresetShaders
        {
            public static string ModelVS = @"#version 330
                    in layout (location = 0) vec3 vertPos;
                    in layout (location = 1) vec3 vertNormal;
                    in layout (location = 2) vec2 vertTexcoord;

                    uniform mat4 viewMat, projMat, worldMat;

                    out vec3 posEye, normalEye;
                    out vec2 texcoord;

                    void main() {
                        posEye = vec3(viewMat * worldMat * vec4(vertPos, 1.0));
                        normalEye = vec3(viewMat * worldMat * vec4(vertNormal, 0.0));
                        texcoord = vertTexcoord;

                        gl_Position = projMat * vec4(posEye, 1.0);
                    }";

            public static string ModelFS = @"#version 330
                    in vec3 posEye, normalEye;
                    in vec2 texcoord;

                    uniform mat4 viewMat;
                    uniform sampler2D texture;
                    uniform vec3 Ks;
                    uniform vec3 Ka;
                    uniform float specWeight;

                    out vec4 fragColor;

                    vec3 lightPos = vec3(10, 10, 0);
                    vec3 Ld = vec3(0.7, 0.7, 0.7);
                    vec3 Ls = vec3(1.0, 1.0, 1.0);
                    vec3 La = vec3(0.2, 0.2, 0.2);

                    void main() {
                        vec3 Ia = La * Ka;

                        vec3 lightPosEye = vec3(viewMat * vec4(lightPos, 1.0));
                        vec3 dirToLightEye = normalize(lightPosEye - posEye);
                        float diffuseDot = max(dot(dirToLightEye, normalEye), 0.0);
                        vec3 Id = Ld * diffuseDot;

                        vec3 refEye = normalize(reflect(-dirToLightEye, normalEye));
                        vec3 surfToViewerEye = normalize(-posEye);
                        float specFactor = pow(max(dot(refEye, surfToViewerEye), 0.0), specWeight);
                        vec3 Is = Ls * Ks * specFactor;

                        fragColor = texture2D(texture, texcoord) * vec4(Id + Is + Ia, 1.0);
                    }";
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                shader.Dispose();
            }

            disposed = true;
        }
    }
}
