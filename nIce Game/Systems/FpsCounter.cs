﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace nIceGame
{
    public class FpsCounter : ISystem
    {
        Text fpsText;
        int updateFrames, drawFrames;
        double time;

        public FpsCounter()
        {
            fpsText = new Text("");

            Entity fpsEntity = new Entity();
            fpsEntity.Set(fpsText);
            fpsEntity.Set(new Transform(new Vector3(20, 20, 0), Quaternion.Identity, Vector3.One));
            Engine.Entities.Add(fpsEntity);

            updateFrames = 0;
            drawFrames = 0;
            time = 0;
        }

        public void OnResize()
        {

        }

        public void OnUpdate(double delta)
        {
            updateFrames++;

            time += delta;
            if (time > 1)
            {
                fpsText.String = "Update FPS: " + updateFrames + "\nDraw FPS: " + drawFrames;
                time -= 1;
                updateFrames = 0;
                drawFrames = 0;
            }
        }

        public void OnRender(double delta)
        {
            drawFrames++;
        }

        public virtual void Dispose()
        {
        }
    }
}
