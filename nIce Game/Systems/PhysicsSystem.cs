﻿using System;
using System.Linq;
using System.Collections.Generic;
using OpenTK;
using BulletSharp;

namespace nIceGame
{
    public class PhysicsSystem : ISystem
    {
        List<Entity> colliderCache;
        bool disposed = false;

        BroadphaseInterface broadphase;
        DefaultCollisionConfiguration collideConfig;
        CollisionDispatcher dispatcher;
        SequentialImpulseConstraintSolver solver;
        DiscreteDynamicsWorld dynamicsWorld;

        public PhysicsSystem()
        {
            colliderCache = new List<Entity>();

            broadphase = new DbvtBroadphase();
            collideConfig = new DefaultCollisionConfiguration();
            dispatcher = new CollisionDispatcher(collideConfig);
            GImpactCollisionAlgorithm.RegisterAlgorithm(dispatcher);
            solver = new SequentialImpulseConstraintSolver();
            dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, broadphase, solver, collideConfig);
            dynamicsWorld.Gravity = new Vector3(0, -9.8f, 0);
        }

        public void OnResize()
        {

        }

        public void OnUpdate(double delta)
        {
            if (!Engine.UpdateCache.ContainsKey(typeof(Collider)) || Engine.UpdateCache[typeof(Collider)])
                UpdateColliderCache();

            dynamicsWorld.StepSimulation(1 / 60f, 10, 1 / 60f);
            for (int i = 0; i < colliderCache.Count; i++)
                colliderCache[i].Get<Transform>().Matrix = colliderCache[i].Get<Collider>().Offset.Matrix * colliderCache[i].Get<Collider>().RigidBody.MotionState.WorldTransform;
        }

        private void UpdateColliderCache()
        {
            List<Entity> newCache = Engine.Entities.FindAll(x => x.Get<Collider>() != null);

            IEnumerable<Entity> lostCache = colliderCache.Except(newCache);
            foreach (Entity entity in lostCache)
                dynamicsWorld.RemoveRigidBody(entity.Get<Collider>().RigidBody);

            IEnumerable<Entity> gainCache = newCache.Except(colliderCache);
            foreach (Entity entity in gainCache)
                dynamicsWorld.AddRigidBody(entity.Get<Collider>().RigidBody);

            colliderCache = newCache;

            Engine.UpdateCache[typeof(Collider)] = false;
        }

        public void OnRender(double delta)
        {

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                dynamicsWorld.Dispose();
                solver.Dispose();
                dispatcher.Dispose();
                collideConfig.Dispose();
                broadphase.Dispose();
            }

            disposed = true;
        }
    }
}
