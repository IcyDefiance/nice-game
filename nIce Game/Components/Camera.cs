﻿using OpenTK;

namespace nIceGame
{
    public class Camera
    {
        public bool IsRendering { get; set; }
        private Matrix4 projection;
        private int minRange;
        private int maxRange;
        private float fieldOfView;
        private Rectangle outputZone;
        private bool updateProj = true;

        public Camera(int minRange, int maxRage, float fieldOfView, Rectangle outputZone, bool isRendering = true)
        {
            MinRange = minRange;
            MaxRange = maxRage;
            FieldOfView = fieldOfView;
            OutputZone = outputZone;
            IsRendering = isRendering;
        }

        public int MinRange
        {
            get { return minRange; }
            set { minRange = value; updateProj = true; }
        }

        public int MaxRange
        {
            get { return maxRange; }
            set { maxRange = value; updateProj = true; }
        }

        public float FieldOfView
        {
            get { return fieldOfView; }
            set { fieldOfView = value; updateProj = true; }
        }

        public Rectangle OutputZone
        {
            get { return outputZone; }
            set { outputZone = value; updateProj = true; }
        }

        public Matrix4 Projection
        {
            get
            {
                if (!updateProj)
                {
                    return projection;
                }
                else
                {
                    projection = Matrix4.CreatePerspectiveFieldOfView(FieldOfView, (float)OutputZone.Width / OutputZone.Height, MinRange, MaxRange);
                    updateProj = false;
                    return projection;
                }
            }
        }
    }
}
