﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace nIceGame
{
    public class BlendFunction
    {
        internal BlendingFactorSrc blendingFactorSrc;
        internal BlendingFactorDest blendingFactorDest;

        public BlendFunction(BlendingFactorSrc src, BlendingFactorDest dest)
        {
            blendingFactorSrc = src;
            blendingFactorDest = dest;
        }
    }
}
