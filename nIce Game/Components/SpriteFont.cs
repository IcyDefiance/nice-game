﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Drawing.Drawing2D;

namespace nIceGame
{
    public class SpriteFont : IDisposable
    {
        public Dictionary<char, Glyph> Glyphs { get; private set; }
        public int AdvanceHeight { get; private set; }
        private int pxSize = 24;
        private bool disposed = false;

        public SpriteFont()
        {
            GlyphTypeface gtf = new GlyphTypeface(new Uri(@"C:\Windows\Fonts\arial.ttf"));
            Glyphs = new Dictionary<char, Glyph>();
            AdvanceHeight = (int)Math.Round(gtf.AdvanceHeights[gtf.CharacterToGlyphMap[78]] * pxSize, MidpointRounding.AwayFromZero);

            for (char i = (char)32; i < 127; i++)
            {
                using (Bitmap bmp = new Bitmap((int)(Math.Round((gtf.LeftSideBearings[gtf.CharacterToGlyphMap[i]] + gtf.AdvanceWidths[gtf.CharacterToGlyphMap[i]] + gtf.RightSideBearings[gtf.CharacterToGlyphMap[i]]) * pxSize * 1.5, MidpointRounding.AwayFromZero)), (int)(Math.Round(gtf.Height * pxSize * 1.5, MidpointRounding.AwayFromZero))))
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.FillRectangle(System.Drawing.Brushes.Transparent, new Rectangle(0, 0, bmp.Width, bmp.Height));
                    g.DrawString(i.ToString(), new Font("Arial", pxSize, GraphicsUnit.Pixel), System.Drawing.Brushes.White, 0, 0);
                    g.Flush();
                    Glyphs[i] = new Glyph(new Texture2D(bmp), Math.Round(gtf.AdvanceWidths[gtf.CharacterToGlyphMap[i]] * pxSize, MidpointRounding.AwayFromZero));
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                foreach (KeyValuePair<char, Glyph> entry in Glyphs)
                    entry.Value.Dispose();
            }

            disposed = true;
        }

        ~SpriteFont()
        {
            Dispose(false);
        }
    }
}
