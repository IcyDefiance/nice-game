﻿using System;
using System.Collections.Generic;
using OpenTK;
using BulletSharp;

namespace nIceGame
{
    public class PlaneCollider : Collider
    {
        public PlaneCollider(Entity parent, Transform offset, float mass, Vector3 normal)
            : base(parent, offset, mass, new StaticPlaneShape(normal, 0))
        {

        }
    }
}
