﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace nIceGame
{
    public class Texture2D : IDisposable
    {
        public static readonly Texture2D WhitePixel = new Texture2D(1, 1) { IsSprite = true };

        public int TexID { get; private set; }
        public int Width { get { return pixels.GetLength(1); } }
        public int Height { get { return pixels.GetLength(0); } }
        public bool IsSprite { get; set; }

        private Color4[,] pixels;
        private bool disposed = false;

        public Texture2D(int width, int height) : this(width, height, Color4.White) { }
        public Texture2D(int width, int height, Color4 color)
        {
            pixels = new Color4[width, height];
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    pixels[x, y] = color;

            SendToGPU();
        }

        public Texture2D(Bitmap bmp, bool sendToGPU = true)
        {
            pixels = new Color4[bmp.Height, bmp.Width];
            
            for (int x = 0; x < bmp.Width; x++)
                for (int y = 0; y < bmp.Height; y++)
                    pixels[bmp.Height - 1 - y, x] = bmp.GetPixel(x, y);

            if (sendToGPU)
                SendToGPU();
        }

        public void SendToGPU()
        {
            if (TexID != 0)
                GL.DeleteTexture(TexID);
            TexID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, TexID);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, pixels.GetLength(1), pixels.GetLength(0), 0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.Float, pixels);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                GL.DeleteTexture(TexID);
            }

            TexID = 0;
            disposed = true;
        }

        ~Texture2D()
        {
            Dispose(false);
        }
    }
}
