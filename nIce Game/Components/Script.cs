﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceGame
{
    public abstract class Script
    {
        protected Entity Parent;

        public Script(Entity parent)
        {
            Parent = parent;
        }

        public abstract void Update(double delta);
        public abstract void Draw(double delta);

        public void Die()
        {
            Parent.Get<List<Script>>().Remove(this);
        }
    }
}
