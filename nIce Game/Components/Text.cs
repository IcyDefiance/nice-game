﻿using System;
using OpenTK;

namespace nIceGame
{
    public class Text
    {
        public Vector4 Color { get; set; }
        public String String { get; set; }

        public Text(string text) : this(text, Vector4.One) { }
        public Text(string text, Vector4 color)
        {
            String = text;
            Color = color;
        }
    }
}
