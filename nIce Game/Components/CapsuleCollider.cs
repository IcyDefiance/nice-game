﻿using System;
using System.Collections.Generic;
using OpenTK;
using BulletSharp;

namespace nIceGame
{
    public class CapsuleCollider : Collider
    {
        public CapsuleCollider(Entity parent, Transform offset, float mass, float radius, float height)
            : base(parent, offset, mass, new CapsuleShape(radius, height))
        {

        }
    }
}
