﻿using System;
using System.Collections.Generic;
using BulletSharp;

namespace nIceGame
{
    public class SphereCollider : Collider
    {
        public SphereCollider(Entity parent, Transform offset, float mass, float radius)
            : base(parent, offset, mass, new SphereShape(radius))
        {

        }
    }
}
