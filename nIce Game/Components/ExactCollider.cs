﻿using System;
using System.Linq;
using System.Collections.Generic;
using BulletSharp;

namespace nIceGame
{
    public class ExactCollider : Collider
    {
        public ExactCollider(Entity parent, Transform offset, float mass)
            : base(parent, offset, mass, new BvhTriangleMeshShape(new TriangleIndexVertexArray(parent.Get<Mesh>().Indices, parent.Get<Mesh>().Vertices.Select(x => x.Vertex).ToArray()), true))
        {

        }
    }
}
