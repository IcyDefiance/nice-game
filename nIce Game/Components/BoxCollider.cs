﻿using System;
using System.Collections.Generic;
using BulletSharp;

namespace nIceGame
{
    public class BoxCollider : Collider
    {
        public BoxCollider(Entity parent, Transform offset, float mass, float boxHalfExtentsX, float boxHalfExtentsY, float boxHalfExtentsZ)
            : base(parent, offset, mass, new BoxShape(boxHalfExtentsX, boxHalfExtentsY, boxHalfExtentsZ))
        {

        }
    }
}
