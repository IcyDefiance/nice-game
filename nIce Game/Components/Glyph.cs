﻿using System;
using System.Drawing;

namespace nIceGame
{
    public class Glyph : IDisposable
    {
        public Texture2D Texture { get; private set; }
        public double AdvanceWidth { get; private set; }
        private bool disposed = false;

        public Glyph(Texture2D texture, double advanceWidth)
        {
            Texture = texture;
            AdvanceWidth = advanceWidth;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                Texture.Dispose();
            }

            disposed = true;
        }

        ~Glyph()
        {
            Dispose(false);
        }
    }
}
