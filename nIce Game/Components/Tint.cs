﻿using System;
using OpenTK;
using OpenTK.Graphics;

namespace nIceGame
{
    public class Tint
    {
        private Vector4 vec4;

        public Tint(float R, float G, float B, float A)
        {
            vec4 = new Vector4(R, G, B, A);
        }

        public Tint(Color4 color)
        {
            vec4 = new Vector4(color.R, color.G, color.B, color.A);
        }

        public Vector4 Vec4
        {
            get { return vec4; }
        }

        public float R
        {
            get { return vec4.X; }
            set { vec4.X = value; }
        }

        public float G
        {
            get { return vec4.Y; }
            set { vec4.Y = value; }
        }

        public float B
        {
            get { return vec4.Z; }
            set { vec4.Z = value; }
        }

        public float W
        {
            get { return vec4.W; }
            set { vec4.W = value; }
        }

        public static readonly Tint White = new Tint(Color4.White);
        public static readonly Tint Black = new Tint(Color4.Black);
        public static readonly Tint Transparent = new Tint(Color4.Transparent);
    }
}
