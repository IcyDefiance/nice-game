﻿using System;
using OpenTK;

namespace nIceGame
{
    public class Transform
    {
        public event EventHandler PositionChanged;

        private Vector3 position;
        private Quaternion rotation;
        private Vector3 scale;
        private Matrix4 matrix, inverseMatrix;
        private bool updateMatrix, updateOther;

        public Transform() : this(Vector3.Zero, Quaternion.Identity, Vector3.One) { }
        public Transform(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        private void UpdateMatrices()
        {
            matrix = Matrix4.CreateScale(scale) * Matrix4.CreateFromQuaternion(rotation) * Matrix4.CreateTranslation(position);
            inverseMatrix = matrix.Inverted();
            updateMatrix = false;
        }

        private void UpdateOther()
        {
            position = matrix.ExtractTranslation();
            rotation = matrix.ExtractRotation();
            scale = matrix.ExtractScale();
            inverseMatrix = matrix.Inverted();
            updateOther = false;
        }

        public Vector3 Position
        {
            get
            {
                if (updateOther)
                    UpdateOther();

                return position;
            }
            set
            {
                position = value;
                updateMatrix = true;
                if (PositionChanged != null)
                    PositionChanged(this, new EventArgs());
            }
        }

        public Quaternion Rotation
        {
            get
            {
                if (updateOther)
                    UpdateOther();

                return rotation;
            }
            set { rotation = value; updateMatrix = true; }
        }

        public Vector3 Scale
        {
            get
            {
                if (updateOther)
                    UpdateOther();
                
                return scale;
            }
            set { scale = value; updateMatrix = true; }
        }

        public Matrix4 Matrix
        {
            get
            {
                if (updateMatrix)
                {
                    UpdateMatrices();
                }

                return matrix;
            }
            set { matrix = value; updateOther = true; }
        }

        public Matrix4 InverseMatrix
        {
            get
            {
                if (updateMatrix)
                    UpdateMatrices();
                else if (updateOther)
                    UpdateOther();

                return inverseMatrix;
            }
        }
    }
}
