﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using BulletSharp;

namespace nIceGame
{
    public abstract class Collider : System.IDisposable
    {
        public enum Shape
        {
            Sphere,
            Box
        }

        CollisionShape shape;
        DefaultMotionState motionState;
        RigidBodyConstructionInfo rigidBodyCI;
        RigidBody rigidBody;
        Transform offset;

        bool disposed = false;

        public Collider(Entity parent, Transform offset, float mass, CollisionShape shape)
        {
            this.offset = offset;
            this.shape = shape;
            motionState = new DefaultMotionState(parent.Get<Transform>().Matrix);
            rigidBodyCI = new RigidBodyConstructionInfo(mass, motionState, shape, shape.CalculateLocalInertia(mass));
            rigidBody = new RigidBody(rigidBodyCI);
        }

        public RigidBody RigidBody
        {
            get { return rigidBody; }
        }

        public Transform Offset
        {
            get { return offset; }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                rigidBody.Dispose();
                rigidBodyCI.Dispose();
                motionState.Dispose();
                shape.Dispose();
            }

            disposed = true;
        }
    }
}