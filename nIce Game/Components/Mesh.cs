﻿using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace nIceGame
{
    public class Mesh : IDisposable
    {
        private MeshElement[] vertices;
        private int[] indices;
        private int vbo, ebo, vao;
        private bool disposed = false;

        public Mesh(MeshElement[] vertices, int[] indices)
        {
            this.vertices = vertices;
            this.indices = indices;

            vbo = GL.GenBuffer();
            ebo = GL.GenBuffer();
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (System.IntPtr)(vertices.Length * vertices[0].SizeInBytes), vertices, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (System.IntPtr)(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, BlittableValueType.StrideOf(vertices), 0);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, BlittableValueType.StrideOf(vertices), Vector3.SizeInBytes);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, BlittableValueType.StrideOf(vertices), Vector3.SizeInBytes * 2);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    GL.DeleteBuffer(vbo);
                    GL.DeleteBuffer(ebo);
                    GL.DeleteVertexArray(vao);
                }

                disposed = true;
            }
        }

        ~Mesh()
        {
            Dispose(false);
        }

        internal MeshElement[] Vertices
        {
            get { return vertices; }
        }

        internal int[] Indices
        {
            get { return indices; }
        }

        public int ElementCount
        {
            get { return indices.Length; }
        }

        public int VAO
        {
            get { return vao; }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MeshElement : ISizeDecl
    {
        public Vector3 Vertex;
        public Vector3 Normal;
        public Vector2 TexCoord;

        public int SizeInBytes
        {
            get
            {
                return (Vector3.SizeInBytes * 2) + Vector2.SizeInBytes;
            }
        }
    }
}
