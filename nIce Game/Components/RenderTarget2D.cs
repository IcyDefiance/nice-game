﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace nIceGame
{
    public class RenderTarget2D : Texture2D
    {
        public static Vector2 CurrentTargetSize { get; private set; }

        private int fb, rb;
        private bool disposed = false;

        public RenderTarget2D(int width, int height)
            : base(width, height, Color4.Transparent)
        {
            rb = GL.GenRenderbuffer();
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, rb);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent32f, width, height);

            fb = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fb);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, TexID, 0);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, rb);
            GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposed)
                return;

            if (disposing)
            {
                GL.DeleteRenderbuffer(rb);
                GL.DeleteFramebuffer(fb);
            }

            disposed = true;
        }

        public static void BindTarget(RenderTarget2D target)
        {
            if (target == null)
            {
                GL.Viewport(Engine.GameWindow.ClientRectangle);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                CurrentTargetSize = new Vector2(Engine.GameWindow.Width, Engine.GameWindow.Height);
            }
            else
            {
                GL.Viewport(0, 0, target.Width, target.Height);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, target.fb);
                CurrentTargetSize = new Vector2(target.Width, target.Height);
            }
        }
    }
}
