﻿using System;
using OpenTK;

namespace nIceGame
{
    public class Material : IDisposable
    {
        public Vector3 AmbientColor { get; set; }
        public Vector3 SpecularColor { get; set; }
        public float SpecularWeight { get; set; }
        public float Transparency { get; set; }

        public Texture2D DiffuseMap { get; set; }

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                DiffuseMap.Dispose();
            }

            disposed = true;
        }

        ~Material()
        {
            Dispose(false);
        }
    }
}
