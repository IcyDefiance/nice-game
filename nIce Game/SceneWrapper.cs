﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceGame
{
    public class SceneWrapper
    {
        public Scene Scene { get; internal set; }
        public Dictionary<Type, bool> UpdateCache { get; internal set; }
        public List<ISystem> Systems { get; internal set; }
        public EntityList Entities { get; internal set; }
    }
}
