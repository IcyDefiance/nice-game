﻿using System;
using System.IO;
using System.Text;
using System.Drawing;
using OpenTK;

namespace nIceGame
{
    public static class Content
    {
        public static Mesh LoadMesh(string fileName)
        {
            fileName = "content\\" + fileName + ".ncf";

            using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open), Encoding.ASCII))
            {
                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] rightSig = asen.GetBytes("nIceGame");
                byte[] signature = reader.ReadBytes(8);
                for (int i = 0; i < 8; i++)
                    if (signature[i] != rightSig[i])
                        throw new IOException("File unreadable.");

                UInt16 fileVersion = reader.ReadUInt16();
                if (fileVersion != 0)
                    throw new IOException("File version " + fileVersion + " not supported.");

                byte[] contentType = reader.ReadBytes(3);
                if (contentType[0] != 'M' || contentType[1] != 'D' || contentType[2] != 'L')
                    throw new IOException("Not a model file.");

                UInt16 modelVersion = reader.ReadUInt16();
                if (modelVersion != 0)
                    throw new IOException("Model version not supported.");

                return loadMdl0(reader);
            }
        }
        private static Mesh loadMdl0(BinaryReader reader)
        {
            MeshElement[] vertices = new MeshElement[reader.ReadInt32()];
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = new MeshElement();
                vertices[i].Vertex = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                vertices[i].Normal = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                vertices[i].TexCoord = new Vector2(reader.ReadSingle(), reader.ReadSingle());
            }

            int[] indices = new int[reader.ReadInt32()];
            for (int i = 0; i < indices.Length; i++)
                indices[i] = reader.ReadInt32();

            return new Mesh(vertices, indices);
        }

        public static Texture2D LoadTexture2D(string fileName, bool isSprite = false, bool preload = false)
        {
            Texture2D result;

            using (Bitmap bmp = new Bitmap("content\\" + fileName + ".png"))
                result = new Texture2D(bmp, !preload);

            result.IsSprite = isSprite;
            return result;
        }
    }
}