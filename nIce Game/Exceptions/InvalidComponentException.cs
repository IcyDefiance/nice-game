﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceGame
{
    [Serializable]
    public class InvalidComponentException : Exception
    {
        public InvalidComponentException() { }
        public InvalidComponentException(string message) : base(message) { }
        public InvalidComponentException(string message, Exception inner) : base(message, inner) { }
        protected InvalidComponentException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
