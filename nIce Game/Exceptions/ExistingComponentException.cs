﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceGame
{
    [Serializable]
    public class ExistingComponentException : Exception
    {
        public ExistingComponentException() { }
        public ExistingComponentException(string message) : base(message) { }
        public ExistingComponentException(string message, Exception inner) : base(message, inner) { }
        protected ExistingComponentException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
