﻿using System;
using System.Collections.Generic;

namespace nIceGame
{
    public class EntityList : List<Entity>
    {
        public new void Add(Entity item)
        {
            foreach (var kv in item.components)
            {
                Engine.UpdateCache[kv.Key] = true;
            }

            base.Add(item);
        }

        public new void AddRange(IEnumerable<Entity> collection)
        {
            foreach (var item in collection)
            {
                foreach (var kv in item.components)
                {
                    Engine.UpdateCache[kv.Key] = true;
                }
            }

            base.AddRange(collection);
        }

        public new void Insert(int index, Entity item)
        {
            foreach (var kv in item.components)
            {
                Engine.UpdateCache[kv.Key] = true;
            }

            base.Insert(index, item);
        }

        public new void InsertRange(int index, IEnumerable<Entity> collection)
        {
            foreach (var item in collection)
            {
                foreach (var kv in item.components)
                {
                    Engine.UpdateCache[kv.Key] = true;
                }
            }

            base.InsertRange(index, collection);
        }

        public new void Remove(Entity item)
        {
            foreach (var kv in item.components)
            {
                Engine.UpdateCache[kv.Key] = true;
            }

            base.Remove(item);
        }

        public new void RemoveAll(Predicate<Entity> match)
        {
            List<Entity> matched = base.FindAll(match);
            for (int i = 0; i < matched.Count; i++)
            {
                foreach (var kv in matched[i].components)
                {
                    Engine.UpdateCache[kv.Key] = true;
                }
            }

            base.RemoveAll(match);
        }

        public new void RemoveAt(int index)
        {
            foreach (var kv in this[index].components)
            {
                Engine.UpdateCache[kv.Key] = true;
            }

            base.RemoveAt(index);
        }

        public new void RemoveRange(int index, int count)
        {
            for (int i = 0; i < count; i++)
            {
                foreach (var kv in this[i + index].components)
                {
                    Engine.UpdateCache[kv.Key] = true;
                }
            }

            base.RemoveRange(index, count);
        }
    }
}
