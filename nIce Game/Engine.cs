﻿using System;
using System.Linq;
using System.Collections.Generic;
using OpenTK;

namespace nIceGame
{
    public static class Engine
    {
        public static GameWindow GameWindow { get; private set; }
        private static Stack<SceneWrapper> sceneStack;

        public static void Initialize(GameWindow gameWindow)
        {
            GameWindow = gameWindow;
            sceneStack = new Stack<SceneWrapper>();
        }

        public static void PushScene(Scene scene)
        {
            if (sceneStack.Count > 0)
            {
                Scene.OnLoseFocus();
                GameWindow.Resize -= Scene.OnResize;
                GameWindow.UpdateFrame -= Scene.UpdateFrame;
                GameWindow.RenderFrame -= Scene.RenderFrame;
            }

            sceneStack.Push(new SceneWrapper());
            Scene = scene;
            UpdateCache = new Dictionary<Type, bool>();
            Systems = new List<ISystem>();
            Entities = new EntityList();
            GameWindow.Resize += Scene.OnResize;
            GameWindow.UpdateFrame += Scene.UpdateFrame;
            GameWindow.RenderFrame += Scene.RenderFrame;
            Scene.OnLoad();
        }

        public static void PopScene()
        {
            if (sceneStack.Count == 0)
                throw new InvalidOperationException("Scene stack has nothing in it.");

            GameWindow.Resize -= Scene.OnResize;
            GameWindow.UpdateFrame -= Scene.UpdateFrame;
            GameWindow.RenderFrame -= Scene.RenderFrame;
            Scene.Dispose();

            sceneStack.Pop();

            if (sceneStack.Count > 0)
            {
                GameWindow.Resize += Scene.OnResize;
                GameWindow.UpdateFrame += Scene.UpdateFrame;
                GameWindow.RenderFrame += Scene.RenderFrame;
                Scene.OnRegainFocus();
            }
        }

        public static Scene Scene
        {
            get
            {
                return sceneStack.Peek().Scene;
            }
            internal set
            {
                sceneStack.Peek().Scene = value;
            }
        }

        public static Dictionary<Type, bool> UpdateCache
        {
            get
            {
                return sceneStack.Peek().UpdateCache;
            }
            internal set
            {
                sceneStack.Peek().UpdateCache = value;
            }
        }

        public static List<ISystem> Systems
        {
            get
            {
                return sceneStack.Peek().Systems;
            }
            internal set
            {
                sceneStack.Peek().Systems = value;
            }
        }

        public static EntityList Entities
        {
            get
            {
                return sceneStack.Peek().Entities;
            }
            internal set
            {
                sceneStack.Peek().Entities = value;
            }
        }
    }
}
