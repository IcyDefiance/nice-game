﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceGame
{
    public class Entity
    {
        public string Name { get; set; }
        internal Dictionary<Type, object> components;

        public Entity()
        {
            components = new Dictionary<Type, object>();
        }

        public void Set<T>(T component)
        {
            components[typeof(T)] = component;
            Engine.UpdateCache[typeof(T)] = true;
        }

        public T Get<T>()
        {
            object result;
            return components.TryGetValue(typeof(T), out result) ? (T)result : default(T);
        }

        public void Remove<T>()
        {
            components.Remove(typeof(T));
            Engine.UpdateCache[typeof(T)] = true;
        }
    }
}
