﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using nIceGame;

namespace Game
{
    class MainScene : Scene
    {
        public override void OnLoad()
        {
            GL.Enable(EnableCap.CullFace);
            GL.Enable(EnableCap.DepthTest);
            Engine.GameWindow.VSync = VSyncMode.Off;

            //systems
            Engine.Systems.Add(new MeshRenderer());
            Engine.Systems.Add(new SpriteRenderer());
            Engine.Systems.Add(new FpsCounter());
            Engine.Systems.Add(new ScriptRunner());

            //cameras
            Entity cameraMain = new Entity();
            cameraMain.Set(new Camera(1, 500, (float)(Math.PI / 3), Engine.GameWindow.ClientRectangle));
            cameraMain.Set(new Transform(new Vector3(0, 1, 3), Quaternion.FromAxisAngle(Vector3.UnitX, (float)(-Math.PI * 20 / 180)), Vector3.One));
            Engine.Entities.Add(cameraMain);

            //models
            Material basicMaterial = new Material();
            basicMaterial.SpecularColor = Vector3.One;
            basicMaterial.AmbientColor = Vector3.One;
            basicMaterial.SpecularWeight = 10;
            basicMaterial.DiffuseMap = Content.LoadTexture2D("colors");

            Entity paddleModel = new Entity();
            Mesh paddleMesh = Content.LoadMesh("paddle");
            paddleModel.Set(paddleMesh);
            paddleModel.Set(basicMaterial);
            paddleModel.Set(new Transform(new Vector3(-1.4f, 0, 0), Quaternion.Identity, Vector3.One));
            List<Script> paddleScripts = new List<Script>();
            paddleScripts.Add(new RotateScript(paddleModel));
            paddleModel.Set(paddleScripts);
            Engine.Entities.Add(paddleModel);

            Entity sphereModel = new Entity();
            Mesh sphereMesh = Content.LoadMesh("sphere");
            sphereModel.Set(sphereMesh);
            sphereModel.Set(basicMaterial);
            sphereModel.Set(new Transform(new Vector3(1.6f, 0, 0), Quaternion.Identity, Vector3.One));
            List<Script> sphereScripts = new List<Script>();
            sphereScripts.Add(new RotateScript(sphereModel));
            sphereModel.Set(sphereScripts);
            Engine.Entities.Add(sphereModel);

            //sprites
            Entity testSprite = new Entity();
            testSprite.Set(Content.LoadTexture2D("colorsSmall", true));
            testSprite.Set(new Transform(new Vector3(100, 100, 0), Quaternion.Identity, Vector3.One));
            Engine.Entities.Add(testSprite);
        }

        public override void OnResize(object sender, EventArgs e)
        {
            base.OnResize(sender, e);
        }

        public override void UpdateFrame(object sender, OpenTK.FrameEventArgs e)
        {
            base.UpdateFrame(sender, e);

            if (Engine.GameWindow.Keyboard[Key.Escape])
                Engine.GameWindow.Exit();
        }

        public override void RenderFrame(object sender, OpenTK.FrameEventArgs e)
        {
            GL.ClearColor(0.1f, 0.1f, 0.1f, 1.0f);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            base.RenderFrame(sender, e);

            Engine.GameWindow.SwapBuffers();
        }
    }
}
