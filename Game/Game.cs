﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using OpenTK.Input;
using nIceGame;

namespace Game
{
    class Game : GameWindow
    {
        public Game()
            : base(1440, 720, GraphicsMode.Default, "nIce Game")
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
        }

        [STAThread]
        static void Main()
        {
            using (Game game = new Game())
            {
                Engine.Initialize(game);
                Engine.PushScene(new MainScene());
                game.Run(60);
                Engine.PopScene();
            }
        }
    }
}