﻿using System;
using nIceGame;
using OpenTK;

namespace Game
{
    class RotateScript : Script
    {
        public RotateScript(Entity parent) : base(parent)
        {
        }

        double totalTime = 0;

        public override void Update(double delta)
        {
            totalTime += delta;

            Transform transform = Parent.Get<Transform>();
            transform.Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, (float)Math.Sin(totalTime));
        }

        public override void Draw(double delta)
        {

        }
    }
}
